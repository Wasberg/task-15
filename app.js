let answer = Math.round(Math.random() *100);


const gBtn = document.getElementById("guessBtn");

gBtn.addEventListener("click", function(){
    let guess = document.getElementById("guess").value;
    if (guess < answer) {
        document.getElementById("response").innerHTML = "Too low, guess again!";
    } else if (guess > answer && guess <= 100) {
        document.getElementById("response").innerHTML = "Too high, guess again!";
    } else if (guess > 100){
        document.getElementById("response").innerHTML = "A number between 0 - 100... &#129318";
    }else {
        document.getElementById("response").innerHTML = "Right you are!";
        let resetBtn = document.createElement("button");
        resetBtn.innerHTML = "Play again?";
        document.body.appendChild(resetBtn);
        resetBtn.addEventListener("click", function() {
            location.reload(false);
        });
    }
});

console.log(answer);
